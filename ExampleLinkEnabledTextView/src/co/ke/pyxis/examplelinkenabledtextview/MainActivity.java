package co.ke.pyxis.examplelinkenabledtextview;

import co.ke.pyxis.LinkEnabledTextView.LinkEnabledTextView;
import co.ke.pyxis.LinkEnabledTextView.TextLinkClickListener;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity implements TextLinkClickListener  {
	private LinkEnabledTextView check;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		 String text  =  "This is a #test of regular expressions with http://example.com " +
		 		"links as used in @twitter for performing various operations based on the " +
		 		"links this handles multiple links like"
                +" http://this_is_fun.com and #Awesomess and @Cool";
		 
		 check = (LinkEnabledTextView) findViewById(R.id.text);
		 check.setOnTextLinkClickListener(this);
		    check.gatherLinksForText(text);
		    check.setLinkTextColor(Color.BLUE);

		    MovementMethod m = check.getMovementMethod();
		    if ((m == null) || !(m instanceof LinkMovementMethod)) {
		        if (check.getLinksClickable()) {
		            check.setMovementMethod(LinkMovementMethod.getInstance());
		        }
		    }
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public void onTextLinkClick(View textView, String clickedString)
	{
		Toast.makeText(MainActivity.this, "Hyperlink clicked is :: " + clickedString, Toast.LENGTH_SHORT).show();
	    android.util.Log.v("Hyperlink clicked is :: " + clickedString, "Hyperlink clicked is :: " + clickedString);
	}
}
