package co.ke.pyxis.LinkEnabledTextView;

import android.view.View;

public interface TextLinkClickListener {

    /**
     * This method is called when the TextLink is clicked from LinkEnabledTextView
     *
     * @param textView
     * @param clickedString
     */
    public void onTextLinkClick(View textView, String clickedString);
}
